# Pi-Hole Network-wide Ad Blocker

With the Thanksgiving holiday upon us, it's time to start thinking of Pi!

This was a project I set up in about one hour on Black Friday with minimal effort and high payoff. Instead of hitting the stores for the best sales, I dug through a box of old development boards and found a handful of available Raspberry Pi boards. 

![pi-hole_vector_logo](./Images/pi-hole_vector_logo.png)

#### Enter the Pi-Hole

Pi-Hole is a Linux software stack for various devices, but it is heavily supported on Raspberry Pi hardware. When a Raspberry Pi is set up as a Pi-Hole, it acts as a DNS advertisement black hole for all devices, network-wide. Under the hood, it uses a modified dnsmasq called FTLDNS, cURL, lighttpd, PHP and the AdminLTE Dashboard to block DNS requests for known tracking and advertising domains.

This means that not only are laptops and PCs on the network no-longer seeing advertisement banners, but also iOS and Android devices. 

For example, my wife likes to play Sudoku on her Android phone, but after each puzzle, there is a slew of ads leading to the next level. With the Pi-Hole, she hasn't gotten a single advertisement, and the free app just continues on to the next game.

## SD Card Setup

Download the latest Raspbian Lite image from the main website, unzip, and burn the .img to an SD card using Etcher.

Image Link: https://www.raspberrypi.org/downloads/raspbian/

![etcher](./Images/etcher.png)

Next, add a blank text file to the SD card called *SSH* and remove the .txt extension. This will enable SSH on the Raspberry Pi so that you can remotely log into the device with an SSH client.

![boot-ssh-file](./Images/boot-ssh-file.png)

Install the SD card into the Raspberry Pi, and allow it time to boot and unpack the file system; this should take ~1 minute, depending on the Pi board version.

## Configure the Pi

The first thing I always do is configure the Pi for an application before installing any software. Let us connect to the device using the IP address and SSH; you can typically find the IP address from your routers DHCP leases table. 

```bash
ssh pi@192.168.1.2
```

The default password is ```raspberry```



Our next step is to update Raspbian Lite to make sure we have the latest version, and that the software on the platform is up to date.

```bash
sudo apt-get update
sudo apt-get dist-upgrade
```

Once upgraded, we will start the configuration window and do a few configuration tasks.

```bash
sudo raspi-config
```

Configure the following:

- Change User Password
- Network Options -> Hostname
- Localization Options -> Locale
- Localization Options -> Timezone

(Optional)

- Overclock

The overclocking is not required, and you should only use overclocking on the Pi if you have an actively cooled case. If you are running on a Raspberry Pi version that is not a v1 or v2, you will need to manually overclock the board.

To do this, open up boot configuration:

```bash
sudo nano /boot/config.txt
```

Adjust, un-comment, or add these lines:

```bash
core_freq=500 # GPU Frequency 
arm_freq=1300 # CPU Frequency 
over_voltage=4 #Electric power sent to CPU / GPU (4 = 1.3V) 
disable_splash=1 # Disables the display of the electric alert screen
```

Reboot the Pi device after all the new configurations have been implemented.

```bash
sudo reboot
```

> Note: you can check the overclocking after reboot with the ```lscpu``` command, there will be a CPU max MHz listing.

## Install Pi-Hole

SSH back into your Pi, and now we will set up the Pi-Hole software stack. To start the installation process, run the following commands.

```bash
wget -O basic-install.sh https://install.pi-hole.net
sudo bash basic-install.sh
```



You will get a few prompts for options, be sure to select your main network interface correctly, and your downstream DNS server; I used Cloudflare DNS. Pay attention to the last dialog box, as it will have your login password to the admin console; write it down, or take a photo.

## Check That Pi-Hole is Running

SSH back into the Pi, and change the default Pi-Hole password. This was given to you on the last page of the Pi-Hole installation dialog.

```bash
sudo pihole -a -p
```

Login to the Pi-Hole admin web-page:

http://192.168.1.2/admin

If you see this page and can log in congratulations! You set up a Pi-Hole!

![pi-hole-admin](./Images/pi-hole-admin.png)

## Configuring Your Router

If you've made it this far, excellent! You have a Pi-Hole on your network. Now all that is needed is some configuration within your router to look at the Pi-Hole for DNS resolution on your network. The concept is such that when a device on the network loads DNS content, it hits a DNS server for the actual IP Address; typically, it's your ISPs DNS server. With this setup, all devices on the network will hit the Pi-Hole first, and it'll determine if it's blacklisted or whitelisted before resolving with the upstream DNS servers.

For this part of the setup, I will walk you through configuring your router, but it might not be identical. I use a router stack called pfSense, based on FreeBSD, and it's menu layout tends to be different than most off-the-shelf routers.

- Login to your network router

- Set a static IP Address reservation for the Pi-Hole

- This is fairly straight forward; instead of letting your router lease an IP Address for the Pi-Hole on boot, give it a static one. Mine for this example is 192.168.1.2, the Pi will always boot-up on the network with this IP reservation.

  ![static](./Images/static.png)

- Next, and most importantly, add the Pi-Hole as the primary DNS server for the router. This is located typically under the LAN side DHCP Server settings. The setting will tell the router to use the Pi-Hole for DNS, instead of the ISP's upstream DNS server.

  ![](./Images/dns-server.png)

  > Note for pfSense: Do not enable DNS forwarded, DNS Resolver, and do not add a DNS entry for System > General > Setup > DNS Server Settings. Only use the DHCP Server settings.

- Lastly, we'll want to login to the admin console for the Pi-Hole and add the router as an up-stream DNS server. This will be your routers IP address, mine is 192.168.1.1; leave off the #53, Pi-Hole will add it after saving.

  **Settings -> DNS Tab**

  ![upstream-dns](./Images/upstream-dns.png)



## Testing the Pi-Hole

To see if the Pi-Hole is working, you will need to go to a device on the network. If you are running a Linux box, run the following command.

```bash
nslookup doubleclick.net
```

doubleclick.net is a blacklisted IP Address on the Pi-Hole, so it shouldn't be reachable via a web browser. You'll see by running nslookup command that it's working when the server reports back as the Pi-Holes IP Address and if the doubleclick.net address comes back as 0.0.0.0.

On Windows and other devices, you might need to renew your lease before running the command. To do this, open a CMD prompt and use the following.

```bash
ipconfig /renew
nslookup doubleclick.net
```



## Updating the Pi-Hole

Over time you might need to update the Pi-Hole's listings and software version. This is very simple to do and can be done with just a couple commands.

```bash
pihole -up
```



## Automating the Updates

I don't necessarily want to have to login to the Pi-Hole device to run updates, so it's a great addition to the device to implement an automated update script. In this section, I use my favorite text editor Vim, but you can use nano, gedit, etc.

```bash
sudo vim /bin/pi-hole-update.sh
```

Add these lines to the document, and then save quit.

```bash
#!/bin/sh
sudo apt update
sudo apt -yf dist-upgrade
sudo apt-get -y --purge autoremove
sudo apt-get autoclean
sudo pihole -g -up
sudo pihole -up
```

Use crontab to manage your automated scripts; you can add your automated script to the cronjobs list with this command:

```bash
sudo crontab -e
```

Add this line to the bottom to create a scheduled update job for every weekday at 1am:

```bash
# Update the Pi and Pi-Hole
0 1 * * 1-5 /bin/pi-hole-update.sh 
```

If you'd like the update to also log it's output, change the line to this instead:

```bash
# Update the Pi and Pi-Hole
0 1 * * 1-5 /bin/pi-hole-update.sh >> /home/pi/pi-hole-update-output.log 2>&1
```

In addition to the logging, there are some MAILTO features of cronjobs that are worth investigating. These features would send you an email on the cronjob's output.

## Bypassing Pi-Hole

There might come a time you need to bypass the Pi-Hole DNS for a device on your network; to do this, it's straightforward. In the router's DHCP lease configuration, make a static IP Address lease for your bypassed device, and in the DNS settings of the static lease, enter a different upstream DNS address to override the Pi-Hole; such as ```1.1.1.1``` for Cloudflare DNS.

## Log2Ram (Optional)

If you don't want to kill your Raspberry Pi SD card after a short period, it's worth implementing the Log2Ram package on the Pi-Hole. This will write /var/log/ files to RAM mount instead of directly to the SD card. By default, every day, the CRON will launch a synchronization of the RAM to the folder located on the physical disk.

```bash
curl -Lo log2ram.tar.gz https://github.com/azlux/log2ram/archive/master.tar.gz
tar xf log2ram.tar.gz
cd log2ram-master
chmod +x install.sh && sudo ./install.sh
cd ..
rm -r log2ram-master
sudo reboot
```

More customization features can be found on the repo page: https://github.com/azlux/log2ram

## DNS-Over-HTTPS (Optional)

With regular DNS, requests are sent in plain-text, with no method to detect  tampering or misbehavior. This means that not only can a malicious actor look at all the DNS requests you are making (and therefore what  websites you are visiting), they can also tamper with the response and  redirect your device to resources in their control (such as a fake login page for internet banking).

DNS-Over-HTTPS prevents this by using standard HTTPS requests to retrieve DNS information. This means that  the connection from the device to the DNS server is secure and can not  easily be snooped, monitored, tampered with or blocked.

The Cloudflare DNS we've been using thus far also supports DoH (DNS-Over-HTTPS), and it's DNS service in general has been reported to be upwards of 40x faster than Google DNS based on location.



First, lets install the Cloudflared package

```bash
cd ~
wget https://bin.equinox.io/c/VdrWdbjqyF/cloudflared-stable-linux-arm.tgz
mkdir argo-tunnel
tar -xvzf cloudflared-stable-linux-arm.tgz -C ./argo-tunnel
rm cloudflared-stable-linux-arm.tgz
cd argo-tunnel
./cloudflared --version
```

The ```--version``` should report back something like ```cloudflared version 2018.4.5 (built 2018-04-09-2155 UTC)```. All that's left to do is start Cloudflared with the right config and I will be using screen to run that for now.

```bash
sudo apt-get install screen
screen
sudo ./cloudflared proxy-dns --port 54 --upstream https://1.1.1.1/.well-known/dns-query --upstream https://1.0.0.1/.well-known/dns-query
```



Next configure only the Upstream DNS to be 127.0.0.1#54 to point locally with port 54 on the Pi-Hole admin portal

![upstream-dns-doh](./Images/upstream-dns-doh.png)

Lastly, we need to run Cloudflared as a service, open a file here:

```bash
sudo vim /etc/systemd/system/dnsproxy.service
```

Add these lines:

```bash
[Unit]
Description=CloudFlare DNS over HTTPS Proxy
Wants=network-online.target
After=network.target network-online.target
 
[Service]
ExecStart=/home/pi/argo-tunnel/cloudflared proxy-dns --port 54 --upstream https://1.1.1.1/.well-known/dns-query --upstream https://1.0.0.1/.well-known/dns-query
Restart=on-abort
 
[Install]
WantedBy=multi-user.target
```

Now install this service, and reboot the pi-hole:

```bash
sudo systemctl enable dnsproxy.service
sudo reboot
```

Test DoH functionality by going here on a network device: https://1.1.1.1/help